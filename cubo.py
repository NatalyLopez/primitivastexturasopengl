# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Cubo
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del cubo y sus diferentes transformaciones
# Es llamada por la clase main.
from Texturas import *

class Cubo:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl
        self.tx = Texturas()

    #Función para crear un cubo
    def crearCubo(self,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()

        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        #Definimos vértices
        self.tx.texture_id

        self.gl.glBegin(self.gl.GL_QUADS)

        glTexCoord2f(0, 1)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        glTexCoord2f(1, 1)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        glTexCoord2f(1, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        glTexCoord2f(0, 0)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        glTexCoord2f(0, 1)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        glTexCoord2f(1, 1)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        glTexCoord2f(1, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        glTexCoord2f(0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        glTexCoord2f(0, 1)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        glTexCoord2f(1, 1)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        glTexCoord2f(1, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        glTexCoord2f(0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        glTexCoord2f(0, 1)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        glTexCoord2f(1, 1)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        glTexCoord2f(1, 0)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        glTexCoord2f(0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        glTexCoord2f(0, 1)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        glTexCoord2f(1, 1)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        glTexCoord2f(1, 0)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        glTexCoord2f(0, 0)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        glTexCoord2f(0, 1)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        glTexCoord2f(1, 1)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        glTexCoord2f(1, 0)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        glTexCoord2f(0, 0)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()
